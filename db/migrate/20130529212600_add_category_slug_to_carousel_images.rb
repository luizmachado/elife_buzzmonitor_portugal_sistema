class AddCategorySlugToCarouselImages < ActiveRecord::Migration
  def change
    add_column :carousel_images, :category_slug, :string
  end
end
