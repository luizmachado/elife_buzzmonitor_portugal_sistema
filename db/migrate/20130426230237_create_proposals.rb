class CreateProposals < ActiveRecord::Migration
  def change
    create_table :proposals do |t|
      t.string :name
      t.string :company_name
      t.string :phone_number
      t.string :email_address
      t.boolean :newsletter

      t.timestamps
    end
  end
end
