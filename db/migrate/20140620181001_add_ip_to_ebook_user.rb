class AddIpToEbookUser < ActiveRecord::Migration
  def change
    add_column :ebook_users, :ip, :string
  end
end
