class CreateEbookUsers < ActiveRecord::Migration
  def change
    create_table :ebook_users do |t|
      t.references :ebook
      t.string :name
      t.string :company
      t.string :email
      t.string :phone
      t.boolean :accept_newsletter

      t.timestamps
    end
  end
end
