class AddHeightAndWidthToCarouselImages < ActiveRecord::Migration
  def change
    add_column :carousel_images, :width, :integer
    add_column :carousel_images, :height, :integer
  end
end
