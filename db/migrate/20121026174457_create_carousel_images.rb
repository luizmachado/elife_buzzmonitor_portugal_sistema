class CreateCarouselImages < ActiveRecord::Migration
  def change
    create_table :carousel_images do |t|
      t.integer :priority
      t.string :link
      t.timestamp :deleted_at

      t.timestamps
    end

	add_attachment :carousel_images, :image
  end
end
