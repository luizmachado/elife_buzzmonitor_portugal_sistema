class AddUsaLightboxToCarouselImages < ActiveRecord::Migration
  def change
    add_column :carousel_images, :usa_lightbox, :boolean
  end
end
