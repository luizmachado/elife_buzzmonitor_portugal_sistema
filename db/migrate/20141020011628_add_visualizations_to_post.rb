class AddVisualizationsToPost < ActiveRecord::Migration
  def change
    add_column :posts, :visualizations, :integer
  end
end
