class AddAcceptProposalToEbookUser < ActiveRecord::Migration
  def change
    add_column :ebook_users, :accept_proposal, :boolean
  end
end
