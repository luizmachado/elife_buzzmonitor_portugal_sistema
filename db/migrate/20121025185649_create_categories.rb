class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
