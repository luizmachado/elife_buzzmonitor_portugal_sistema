class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :category
      t.string :title
      t.string :subtitle
      t.text :body
      t.string :slug
      t.integer :author_id
      t.date :publish_date
      t.timestamp :deleted_at

      t.timestamps
    end
    
    add_attachment :posts, :cover

    add_index :posts, :category_id
  end
end
