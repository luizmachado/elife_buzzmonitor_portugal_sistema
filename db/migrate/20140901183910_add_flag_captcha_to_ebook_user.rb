class AddFlagCaptchaToEbookUser < ActiveRecord::Migration
  def change
    add_column :ebook_users, :flag_captcha, :boolean
  end
end
