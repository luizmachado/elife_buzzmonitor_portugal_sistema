class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :ip
      t.boolean :flag_captcha

      t.timestamps
    end
  end
end
