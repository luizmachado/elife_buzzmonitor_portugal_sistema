class AddScriptToEbook < ActiveRecord::Migration
  def change
    add_column :ebooks, :script, :text
  end
end
