class AddMinifiedUrlToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :minified_url, :string
  end
end
