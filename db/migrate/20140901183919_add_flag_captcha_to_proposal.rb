class AddFlagCaptchaToProposal < ActiveRecord::Migration
  def change
    add_column :proposals, :flag_captcha, :boolean
  end
end
