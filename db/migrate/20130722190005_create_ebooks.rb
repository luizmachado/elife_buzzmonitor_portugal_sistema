class CreateEbooks < ActiveRecord::Migration
  def change
    create_table :ebooks do |t|
      t.date :data
      t.string :titulo
      t.string :slug
      t.string :minified_url
      t.text :texto
      t.string :imagem
      t.string :link
      t.timestamp :deleted_at

      t.timestamps
    end

  add_attachment :ebooks, :imagem


  end
end

