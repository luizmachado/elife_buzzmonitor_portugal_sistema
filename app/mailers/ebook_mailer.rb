# encoding: UTF-8

class EbookMailer < ActionMailer::Base
  default from: "contato@buzzmonitor.com.br"

  def notification ebook_user
    @ebook_user = ebook_user
    mail to: Rails.configuration.default_send_mail, :subject => '[ Buzzmonitor Portugal ] Novo solicitação de ebook Portugal'
  end
end
