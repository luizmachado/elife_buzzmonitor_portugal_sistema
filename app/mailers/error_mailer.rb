# encoding: UTF-8

class ErrorMailer < ActionMailer::Base
  default from: "atendimento@casulloweb.com.br"

  def notification_error msg, object
    @object = object
    @msg = msg
    mail to: Rails.configuration.notification_error, :subject => '[ Buzzmonitor Portugal ] Notificação de erro na aplicação Portugal'
  end
end
