# encoding: UTF-8

class PropostaMailer < ActionMailer::Base
  default from: "contacto@buzzmonitor.pt"

  def nova_proposta proposal
    @proposal = proposal
    mail to: Rails.configuration.solicitacao_email_destino, :subject => '[ Buzzmonitor Portugal ] Novo solicitação de proposta Portugal'
  end
end
