$( document ).ready(function(){

load();


  $(".plan-to-proposal").click(function(){
    type = $(this).data("type");
    $("#proposal_origin").val(type);
  });

$(".form-validate-proposal").validate();

   $("#form-signup").submit(function(e) {

      $("#submit-signup").val("Aguarde...");

       var email = $(this).find("#email-signup").val();
       var postData = $(this).serializeArray();
       var formURL = $(this).attr("action");
       $.ajax(
       {
           url : formURL,
           type: "POST",
           data : postData,
           success:function(data, textStatus, jqXHR) 
           {
             
             $.post( "/submit_signup",  { email: email } , function(){
               window.location.href = "https://staging.buzzmonitor.com.br";
             });
               
           },
           error: function(jqXHR, textStatus, errorThrown) 
           {
               response = jQuery.parseJSON(jqXHR.responseText)
               $('.notifications').notify({
                 type: 'notify',
                 message: { text: response.error },
                 closable: false,
                 fadeOut: {enabled: true, delay: 3000}
               }).show();

               $("#submit-signup").val("Use Grátis");
           }
       });
       e.preventDefault(); //STOP default action
   });

});

$(window).load(function(){
    $('#dg-container-gallery').gallery();
});


function load(){
   proposal_captcha();
   ebook_captcha();
   general_captcha();
}

function general_captcha(){
   if ( $("#general-captcha").is(':visible') ){
       Recaptcha.create( recaptcha_public_key, 'general-captcha', {         
         tabindex: 1,
         theme : 'custom',
         lang : 'pt',
         custom_theme_widget: 'recaptcha_widget_general'
      });
       return false;
   }
}

function proposal_captcha(){
   if ( $("#proposal-captcha").is(':visible') ){
       Recaptcha.create( recaptcha_public_key, 'proposal-captcha', {         
         tabindex: 1,
         theme : 'custom',
         lang : 'pt',
         custom_theme_widget: 'recaptcha_widget_proposal'
      });
       return false;
   }
}

function ebook_captcha(){
   if ( $("#ebook-captcha").is(':visible') ){

      Recaptcha.create( recaptcha_public_key, 'ebook-captcha', {         
         tabindex: 1,
         theme : 'custom',
         lang : 'pt',
         custom_theme_widget: 'recaptcha_widget_ebook'
      });

    
       return false;
   }
}



