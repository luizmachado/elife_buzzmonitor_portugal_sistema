class PasswordsController < ActiveAdmin::Devise::PasswordsController
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)

    flash[:success] = t('devise.passwords.send_instructions')

    respond_with({}, :location => after_sending_reset_password_instructions_path_for(resource_name))
  end
end