# encoding: UTF-8
class PagesController < ApplicationController

  def home
    @remote_ebook = cookies[:ebook_user].blank? ? true : false
    @ebook_user = EbookUser.new
    @proposal = Proposal.new
    @user = User.new
    render 'home_b'
  end

  def home_a
    @remote_ebook = cookies[:ebook_user].blank? ? true : false
    @ebook_user = EbookUser.new
    @proposal = Proposal.new
    @user = User.new
    render 'home_a'
  end

  def home_b
    @remote_ebook = cookies[:ebook_user].blank? ? true : false
    @ebook_user = EbookUser.new
    @proposal = Proposal.new
    @user = User.new
    render 'home_b'
  end

  def home_c
    @remote_ebook = cookies[:ebook_user].blank? ? true : false
    @ebook_user = EbookUser.new
    @proposal = Proposal.new
    @user = User.new
    render 'home_c'
  end

  

  def promo_1
  	render layout: "clean"
  end

  def promo_2
  	render layout: "clean"
  end

  def promo_3
    render layout: "clean"
  end

  def promo_4
    render layout: "clean"
  end

  def promo_1_tks
   render layout: "clean"
  end

  def promo_2_tks
   render layout: "clean"
  end

  def promo_3_tks
   render layout: "clean"
  end

  def promo_4_tks
   render layout: "clean"
  end

  def promo_5
    @remote_ebook = cookies[:ebook_user].blank? ? true : false
    @ebook_user = EbookUser.new
    @proposal = Proposal.new
  end

  def promo_5_tks
   render layout: "clean"
  end

  def conheca

  end

  def error_404
    render layout: "clean"
  end

  def faq
  	render :layout => false
  end

  def submit_newsletter
  	
  	@newsletter = Newsletter.new(params[:newsletter])

    respond_to do |format|
      if @newsletter.save
        begin
          mailchimp(@newsletter, t('mailchimp.default_list'))
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @newsletter).deliver
        end
        flash[:notice] = t('newsletter.success')
        format.js 
      else
        format.js
      end

    end

  end

  def submit_signup

    email = params[:email]

        begin
            mailchimp_v2(ENV['mailchimp_list'], email )
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, email).deliver
        end

        begin
          tags = []
          first_name = "User "
          last_name = "Signin"
          tags << 'signin-br'
          tags << 'bm-pt'
          highrise_v2(first_name, last_name, email, tags)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, email).deliver
        end

        @email = email
        ExtraMailer.notification("andaira@casulloweb.com.br, negocios@elifemonitor.com", "[Buzzmonitor BR] Novo sign up", email, "").deliver
     
     render nothing: true

  end

  def submit_proposal

		@proposal = Proposal.new(params[:proposal])

    origin = params[:proposal_origin]

		unless !@proposal_verify_robot
			@proposal.captcha = recaptcha()
      @proposal.flag_captcha  = true
		else
			@proposal.captcha = true
      @proposal.flag_captcha  = false
		end
    @proposal.ip = request.remote_ip

    respond_to do |format|
      if @proposal.save 

        begin
          if @proposal.newsletter
            mailchimp(@proposal, t('mailchimp.default_list') )
          end
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @proposal).deliver
        end

        begin
            tags = []
            unless origin.nil?
              tags << origin
            end
            tags << 'proposta-pt'
            tags << 'bm-pt'
          highrise(@proposal, tags)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @proposal).deliver
        end

        PropostaMailer.nova_proposta(@proposal).deliver
        flash[:notice] = t('proposal.success')

        @proposal = Proposal.new()
     
        format.js 
      else
        format.js
      end
    end

  end

  def submit_proposal_promo_1

		@proposal = Proposal.new(params[:proposal])

    

    if @proposal_verify_robot
      @proposal.captcha = recaptcha()
      @proposal.flag_captcha  = true
    else
      @proposal.captcha = true
      @proposal.flag_captcha  = false
    end
    @proposal.ip = request.remote_ip


    respond_to do |format|

    	if @proposal.save 
    	
    		begin
    			mailchimp(@proposal, t("mailchimp.default_list"))
    		rescue Exception => msg  
    			ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @proposal).deliver
    		end

    		begin

          http_referer = params[:http_referer]
          if http_referer
           facebook = /facebook/.match(http_referer).nil?
           google = /google/.match(http_referer).nil?

           if !facebook
              tag = 'facebook'
           elsif !google 
              tag = 'google'
           else
              tag = 'direto'
           end

            tag = ['PROMO', 'proposta-pt', tag]
          else
            tag = ['PROMO','proposta-pt']
          end

    			highrise(@proposal, tag)
    		rescue Exception => msg  
    			ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @proposal).deliver
    		end

    		PropostaMailer.nova_proposta_promo1(@proposal).deliver
    		flash[:notice] = t('proposal.success')
    		session[:proposal_attempt] = true
        
        format.js 
    	else
    		format.js 
    	end  

    end

  end


  def submit_proposal_promo_2

    @proposal = Proposal.new(params[:proposal])

    if @proposal_verify_robot
      @proposal.captcha = recaptcha()
      @proposal.flag_captcha  = true
    else
      @proposal.captcha = true
      @proposal.flag_captcha  = false
    end
    @proposal.ip = request.remote_ip


    respond_to do |format|

      if @proposal.save 
      
        begin
          mailchimp(@proposal, t("mailchimp.default_list"))
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @proposal).deliver
        end

        begin

          http_referer = params[:http_referer]
          if http_referer
           facebook = /facebook/.match(http_referer).nil?
           google = /google/.match(http_referer).nil?

           if !facebook
              tag = 'facebook'
           elsif !google 
              tag = 'google'
           else
              tag = 'direto'
           end

            tag = ['PROMO2', 'proposta-pt', tag]
          else
            tag = ['PROMO2','proposta-pt']
          end

          highrise(@proposal, tag)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @proposal).deliver
        end

        PropostaMailer.nova_proposta_promo2(@proposal).deliver
        flash[:notice] = t('proposal.success')
        session[:proposal_attempt] = true
        
        format.js 
      else
        format.js 
      end  

    end

  end


  def submit_proposal_promo_3

    @proposal = Proposal.new(params[:proposal])

    if @proposal_verify_robot
      @proposal.captcha = recaptcha()
      @proposal.flag_captcha  = true
    else
      @proposal.captcha = true
      @proposal.flag_captcha  = false
    end
    @proposal.ip = request.remote_ip


    respond_to do |format|

      if @proposal.save 
      
        begin
          mailchimp(@proposal, t("mailchimp.default_list"))
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @proposal).deliver
        end

        begin

          http_referer = params[:http_referer]
          if http_referer
           facebook = /facebook/.match(http_referer).nil?
           google = /google/.match(http_referer).nil?

           if !facebook
              tag = 'facebook'
           elsif !google 
              tag = 'google'
           else
              tag = 'direto'
           end

            tag = ['PROMOYOUTUBE', 'proposta-pt', tag]
          else
            tag = ['PROMOYOUTUBE','proposta-pt']
          end

          highrise(@proposal, tag)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @proposal).deliver
        end

        PropostaMailer.nova_proposta_promo3(@proposal).deliver
        flash[:notice] = t('proposal.success')
        session[:proposal_attempt] = true
        
        format.js 
      else
        format.js 
      end  

    end

  end


  def submit_proposal_promo_4

    @proposal = Proposal.new(params[:proposal])

    if @proposal_verify_robot
      @proposal.captcha = recaptcha()
      @proposal.flag_captcha  = true
    else
      @proposal.captcha = true
      @proposal.flag_captcha  = false
    end
    @proposal.ip = request.remote_ip


    respond_to do |format|

      if @proposal.save 
      
        begin
          mailchimp(@proposal, t("mailchimp.default_list"))
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @proposal).deliver
        end

        begin

          http_referer = params[:http_referer]
          if http_referer
           facebook = /facebook/.match(http_referer).nil?
           google = /google/.match(http_referer).nil?

           if !facebook
              tag = 'facebook'
           elsif !google 
              tag = 'google'
           else
              tag = 'direto'
           end

            tag = ['geracao_digital', 'proposta-pt', tag]
          else
            tag = ['geracao_digital','proposta-pt']
          end

          highrise(@proposal, tag)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @proposal).deliver
        end

        PropostaMailer.nova_proposta_promo4(@proposal).deliver
        flash[:notice] = t('proposal.success')
        session[:proposal_attempt] = true
        
        format.js 
      else
        format.js 
      end  

    end

  end

  def submit_proposal_promo_5

    @proposal = Proposal.new(params[:proposal])

    if @proposal_verify_robot
      @proposal.captcha = recaptcha()
      @proposal.flag_captcha  = true
    else
      @proposal.captcha = true
      @proposal.flag_captcha  = false
    end
    @proposal.ip = request.remote_ip


    respond_to do |format|

      if @proposal.save 
      
        begin
          mailchimp(@proposal, t("mailchimp.default_list"))
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @proposal).deliver
        end

        begin

          http_referer = params[:http_referer]
          if http_referer
           facebook = /facebook/.match(http_referer).nil?
           google = /google/.match(http_referer).nil?

           if !facebook
              tag = 'facebook'
           elsif !google 
              tag = 'google'
           else
              tag = 'direto'
           end

            tag = ['google_ads_bm', 'proposta-pt', tag]
          else
            tag = ['google_ads_bm','proposta-pt']
          end

          highrise(@proposal, tag)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @proposal).deliver
        end

        PropostaMailer.nova_proposta_default('[ Buzzmonitor Portugal ] Nova solicitação de proposta GOOGLE.',@proposal).deliver
        flash[:notice] = t('proposal.success')
        session[:proposal_attempt] = true
        
        format.js 
      else
        format.js 
      end  

    end

  end



  def submit_proposal_promoyoutube

    proposal = Proposal.new(params[:proposal])

      http_referer = params[:http_referer]

      if http_referer
         facebook = /facebook/.match(http_referer).nil?
         google = /google/.match(http_referer).nil?

         if !facebook
            tag = 'facebook'
         elsif !google 
            tag = 'google'
         else
            tag = 'direto'
         end

         tag = ['PROMOYOUTUBE', 'proposta-pt', tag]
      else
         tag = ['PROMOYOUTUBE', 'proposta-pt']
      end

     
      unless !@proposal_verify_robot
         proposal.captcha = Net::HTTP.post_form(
         URI.parse('http://www.google.com/recaptcha/api/verify'),
            {
               'privatekey'=> Rails.application.config.recaptcha_private_key,
               'remoteip' => request.ip,
               'challenge' => params[:recaptcha_challenge_field],
               'response' => params[:recaptcha_response_field],
            }
         ).body.lines.first.chomp == "true"
         proposal.flag_captcha  = true
      else
         proposal.captcha = true
         proposal.flag_captcha  = false
      end
      proposal.ip = request.remote_ip

  if proposal.save 
  
    begin
      mailchimp(proposal, 'Mailing Brasil (atualizado)')
    rescue Exception => msg  
      logger.debug("#DEBUG PROPOSAL: " + msg.inspect)
      flash[:notice] = 'Falha ao enviar formulário. Tente mais tarde.' 
      ErrorMailer.notification_error('Problema no MailChimp do solicitação de Proposta BR: ' + msg.inspect, proposal).deliver
    end

    begin
      highrise(proposal, tag) 
    rescue Exception => msg  
      logger.debug("#DEBUG PROPOSAL: " + msg.inspect)
      flash[:notice] = 'Falha ao enviar formulário. Tente mais tarde.' 
      ErrorMailer.notification_error(t('ebook.highrise_error_subject') + msg.inspect, proposal).deliver
    end

    PropostaMailer.nova_proposta_promoyoutube(proposal).deliver
    flash[:notice] = 'Obrigado pelo interesse, em breve um de nossos consultores entrará em contato.'
    redirect_to promoyoutube_tks_path

  else

    if proposal.errors.any? 
        errors = ""
        proposal.errors.full_messages.each do |msg| 
          errors = msg
        end 
     end 
    flash[:notice] = 'Falha ao enviar formulário. ' + errors 
    redirect_to promoyoutube_path
  end

    session[:proposal_attempt] = true

  end




	

end

