class ArtigosController < ApplicationController
  before_filter { |controller| @title = 'Buzzmonitor - Artigos'}
  before_filter { @proposal_verify_robot = Proposal.verify_robot(request.remote_ip) }

  def index
  	@posts = Post.artigo.order('publish_date DESC').page(params[:page]).per(10)
  	@posts = @posts.where(:publish_date => Date.parse(params[:date])) unless params[:date].nil?
  	@dias_com_post = Post.artigo.dias_com_post
  end

  def show
  	@post = Post.artigo.where(:slug => params[:slug]).first
  	redirect_to artigo_path if @post.nil?

    @posts = Post.artigo.where('publish_date <= ? AND posts.id != ?', @post.publish_date, @post.id).order('publish_date DESC').page(params[:page]).per(10)
    @dias_com_post = Post.artigo.dias_com_post
  end
end
