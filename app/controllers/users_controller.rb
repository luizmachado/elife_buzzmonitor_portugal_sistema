# encoding: UTF-8
class UsersController < ApplicationController
 before_filter { |controller| @title = 'Buzzmonitor - E-books'}
  
  def submit_signup

    @user = User.new(params[:user])

    origin = params[:user_origin]

    unless !@user_verify_robot
      @user.captcha = recaptcha()
      @user.flag_captcha  = true
    else
      @user.captcha = true
      @user.flag_captcha  = false
    end
    @user.ip = request.remote_ip

    respond_to do |format|
      if @user.save 

        begin
          mailchimp_v2(t("mailchimp.default_list"), @user.email )
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @user.email).deliver
        end

        begin
          tags = []
          first_name = "User "
          last_name = "Signup"
          tags << 'signup-pt'
          tags << 'bm-pt'
          highrise_v2(first_name, last_name, @user.email, tags)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @user.email).deliver
        end

        #ExtraMailer.notification("andaira@casulloweb.com.br, negocios@elifemonitor.com", "[Buzzmonitor BR] Novo sign up", @user.email, "").deliver
     
        flash[:notice] = t('user.success')

        @user = User.new()
        format.js 
      else

        format.js
      end

    end


  end

  


end


