class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter { @proposal_verify_robot = Proposal.verify_robot(request.remote_ip) }
  before_filter { @ebook_verify_robot = EbookUser.verify_robot(request.remote_ip) }


 def recaptcha
   Net::HTTP.post_form(
      URI.parse('http://www.google.com/recaptcha/api/verify'),
      {
         'privatekey'=> Rails.application.config.recaptcha_private_key,
         'remoteip' => request.ip,
         'challenge' => params[:recaptcha_challenge_field],
         'response' => params[:recaptcha_response_field],
      }
   ).body.lines.first.chomp == "true"
 end


  def mailchimp object, list_name

      Gibbon::API.api_key = ENV['mailchimp_api_key']
      Gibbon::API.timeout = 15
      Gibbon::API.throws_exceptions = false
      list = Gibbon::API.lists.list({:filters => {:list_name => list_name}})["data"].first
      
         if list.nil?
            raise t("mailchimp.list_not_found")
         end
         
         if object.has_attribute?(:email_address)
            email = object.email_address
         else
            email = object.email
         end
        
         if object.has_attribute?(:name) && !object.name.nil?
            response =  Gibbon::API.lists.subscribe( { :id => list["id"], :email => { :email =>  email },  :merge_vars => {:FNAME => object.first_name, :LNAME => object.last_name } })
         else
            response = Gibbon::API.lists.subscribe( { :id => list["id"], :email => { :email =>  email } })
         end

     return response
     
   end


    def mailchimp_v2 list_name, email, first_name = nil, last_name = nil

      Gibbon::API.api_key = ENV['mailchimp_api_key']
      Gibbon::API.timeout = 15
      Gibbon::API.throws_exceptions = false
      list = Gibbon::API.lists.list({:filters => {:list_name => list_name}})["data"].first
      
         if list.nil?
            raise t("mailchimp.list_not_found")
         end
         
         if email.blank?
           raise t("mailchimp.email_blank")
         end

         if !first_name.nil? && !last_name.nil?
            response =  Gibbon::API.lists.subscribe( { :id => list["id"], :email => { :email =>  email },  :merge_vars => {:FNAME => first_name, :LNAME => last_name } })
         else
            response = Gibbon::API.lists.subscribe( { :id => list["id"], :email => { :email =>  email } })
         end

     return response
     
   end


  def highrise object, tags = nil


      if !object.nil? && !object.name.blank?

         if object.has_attribute?(:email_address)
            email = object.email_address
         else
            email = object.email
         end

         if object.has_attribute?(:company_name)
            company = object.company_name
         else
            company = object.company
         end

         if object.has_attribute?(:phone_number)
            phone = object.phone_number
         else
            phone = object.phone
         end

         person = Highrise::Person.search( :email => email )
         if person.is_a?(Array)
           person = person.first
         end

         if person.nil?
            person = Highrise::Person.new({
               first_name: object.first_name,
               last_name: object.last_name,
               company_name: company ? company : '',
               contact_data: {
                  email_addresses: [
                     email_address: { address: email, location: 'Work' }
                  ],
                  phone_numbers: [
                     phone_number: { number: phone, location: 'Work' }
                  ]
               }
            })
            person.set_field_value("Date", Time.now.strftime("%d/%m/%Y %H:%M:%S") )
            person.save
         end
         
         unless tags.nil?
            if tags.is_a?(Array)
               tags.each do |tag|
                  person.tag!(tag) 
               end
            else
               person.tag!(tags)
            end
         end
         
      else
         true
      end
   end


   def highrise_v2 first_name, last_name, email, tags = nil

         person = Highrise::Person.search( :email => email )
         if person.is_a?(Array)
           person = person.first
         end

         if person.nil?
            person = Highrise::Person.new({
               first_name: first_name,
               last_name: last_name,
               contact_data: {
                  email_addresses: [
                     email_address: { address: email, location: 'Work' }
                  ]
               }
            })
            person.set_field_value("Date", Time.now.strftime("%d/%m/%Y %H:%M:%S") )
            person.save
         end
         
         unless tags.nil?
            if tags.is_a?(Array)
               tags.each do |tag|
                  person.tag!(tag) 
               end
            else
               person.tag!(tags)
            end
         end
         
     
   end





end
