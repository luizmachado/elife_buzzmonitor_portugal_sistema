class ImprensaController < ApplicationController
  before_filter { |controller| @title = 'Buzzmonitor - Imprensa'}
  before_filter { @proposal_verify_robot = Proposal.verify_robot(request.remote_ip) }

  def index
  	@posts = Post.imprensa.order('publish_date DESC').page(params[:page]).per(10)
  	@posts = @posts.where(:publish_date => Date.parse(params[:date])) unless params[:date].nil?
  	@dias_com_post = Post.imprensa.dias_com_post
    @ranking = Post.imprensa.where("visualizations IS NOT NULL").order('visualizations DESC').limit(5)


    respond_to do |format|
      format.rss { render :layout => false }
      format.html 
    end
  end

  def show
    @post = Post.imprensa.where(:slug => params[:slug]).first
    @posts = Post.imprensa.where(:slug => params[:slug]).page(params[:page]).per(10)

    @dias_com_post = Post.imprensa.dias_com_post
    @ranking = Post.imprensa.where("visualizations IS NOT NULL").order('visualizations DESC').limit(5)

    @post.increment_visualizations
  end
end
