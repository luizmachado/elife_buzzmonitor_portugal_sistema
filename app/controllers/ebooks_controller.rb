# encoding: UTF-8
class EbooksController < ApplicationController
 before_filter { |controller| @title = 'Buzzmonitor - E-books'}
 before_filter :list_ebooks, :only => [:index, :register]
  
  def index
    @ebook_user = EbookUser.new
  end

  def show
    redirect_to ebooks_path
  end

  def download
  	@ebook = Ebook.find_by_slug(params[:slug]);

    unless cookies[:ebook_user].blank?
      @ebook_user = EbookUser.find(cookies[:ebook_user])

      unless @ebook_user.nil?
        @ebook_user.ebook = @ebook

        begin
          highrise(@ebook_user, 'EBOOK_PORTUGAL_' + @ebook.slug)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @ebook_user).deliver
        end
        flash[:notice] = t('ebook.waiting')
      end
    end

    respond_to do |format|
      format.js 
      format.html { redirect_to thankyou_ebooks_url(@ebook.slug) }
    end
  end



  def detail
    @ebook = Ebook.find_by_slug(params[:slug]);
    @ebook_user = EbookUser.new

    if @ebook.nil?
      flash[:notice] = 'Ebook não encontrado.'
      redirect_to ebooks_url
    end

  end

  def register
    @ebook = nil
    @ebook_user = EbookUser.new(params[:ebook_user])
    @ebook = Ebook.find(@ebook_user.ebook_id) unless @ebook_user.ebook_id.nil?

    if @ebook_verify_robot
      @ebook_user.captcha = recaptcha()
      @ebook_user.flag_captcha  = true
    else
      @ebook_user.captcha = true
      @ebook_user.flag_captcha  = false
    end
    @ebook_user.ip = request.remote_ip


    respond_to do |format|
      if @ebook_user.save 

        begin
          if @ebook_user.accept_newsletter
            mailchimp(@ebook_user, t('mailchimp.default_list') )
          end
        rescue Exception => msg  
          ErrorMailer.notification_error(t('mailchimp.error_subject') + msg.inspect, @ebook_user).deliver
        end

        begin
          tags = ['buzzmonitor','bm-pt','EBOOK_PORTUGAL']
          if @ebook_user.accept_proposal == true
            tags << 'proposta-pt'
          end
          highrise(@ebook_user, tags)
        rescue Exception => msg  
          ErrorMailer.notification_error(t('highrise.error_subject') + msg.inspect, @ebook_user).deliver
        end

        EbookMailer.notification(@ebook_user).deliver
        flash[:notice] = t('ebook.success')
        cookies[:ebook_user] = @ebook_user.id

        format.js 
      else
        format.js
      end
    end


  end


  def thankyou
    @ebook = Ebook.find_by_slug(params[:slug]);
    if @ebook.nil?
      flash[:notice] = 'Ebook não encontrado.'
      redirect_to ebooks_url 
    end

    unless cookies[:ebook_user].blank?
        @ebook_user = EbookUser.find(cookies[:ebook_user])

      unless @ebook_user.nil?
          @ebook_user.ebook = @ebook
        begin
          tags = ['buzzmonitor','bm-pt','ebook']
          tags << @ebook.slug
          highrise(@ebook_user, tags) 
        rescue Exception => msg  
          logger.debug("#DEBUG EBOOK: " + msg.inspect)
          ErrorMailer.notification_error( t('highrise.error_subject') + msg.inspect, @ebook_user).deliver
        end
        EbookMailer.notification(@ebook_user).deliver
      end
    end

  end


  private

  def list_ebooks
    @ebooks = Ebook.order('data DESC')
  end


  


end


