# encoding: UTF-8
class EbookUser < ActiveRecord::Base

	 belongs_to :ebook

	attr_accessor :captcha, :attempt

	attr_accessible :accept_newsletter, :company, :email, :name, :phone,  :ebook_id, :ip, :accept_proposal, :flag_captcha
	validates_presence_of :email

	validate :captcha_validation


	def captcha_validation
		errors.add(:captcha, "inválido") if self.captcha == false
	end

	 validates :email,
            :format => {
              :with    => /^([^\s]+)((?:[-a-z0-9]\.)[a-z]{2,})$/i,
              :message => "inválido" }

  def first_name
  	unless self.name.blank?
  		name = self.name.split(" ")
			return name[0]
		else
			return ''
		end
  end

  def last_name
  	unless self.name.blank?
	  	name = self.name.split(" ")
	  	if name.count > 1
	  		return name[1]
	  	else
	  		return ''
	  	end
	  else
	  	return ''
	  end
  end

  def self.verify_robot remote_ip 
  	ebooks_users =  find(:all, :conditions => ["ip = ? AND (flag_captcha = ? OR flag_captcha = ?) ", remote_ip , true, false] , :order => "created_at DESC", :limit => 2 )
  	if !ebooks_users.nil? && ebooks_users.count >= 2 && (ebooks_users.find { |ebooks_user| ebooks_user.flag_captcha == true }.nil?)
  		true
  	else
  		false
  	end
  end


  #   def encrypt_id
  #   	secret = Digest::SHA1.hexdigest("yourpass")
		# a = ActiveSupport::MessageEncryptor.new(secret)
		# b = a.encrypt(self.id)
  #   end

  #   def decrypt_id(token)
  #   	secret = Digest::SHA1.hexdigest("yourpass")
  #   	c = ActiveSupport::MessageEncryptor.new(secret)
		# c.decrypt(token)
  #  	end

end



 

