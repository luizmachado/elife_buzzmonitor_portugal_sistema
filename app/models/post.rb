class Post < ActiveRecord::Base
  belongs_to :category

  has_attached_file :cover, :styles => {:home => '160x161#'},
    :path           => "#{ENV['s3_root']}/:attachment/:id/:style.:extension",
    :storage        => :s3,
    :bucket            => ENV['s3_bucket'],
    :s3_credentials => {
        :access_key_id     => ENV['s3_access_key_id'],
        :secret_access_key => ENV['s3_secret_access_key']
    }

  validates_presence_of :category_id, :title, :subtitle, :body, :slug, :publish_date
  
  validates_uniqueness_of :slug

  attr_accessible :author_id, :body, :deleted_at, :publish_date, :slug, :subtitle, :title, :category_id, :cover, :preview, :minified_url

  after_create :generate_minified_url

  scope :blog, joins(:category).where("lower(categories.title) = 'blog' AND publish_date <= '#{Date.current}'").readonly(false)
  scope :imprensa, joins(:category).where("lower(categories.title) = 'imprensa' AND publish_date <= '#{Date.current}'").readonly(false)
  scope :artigo, joins(:category).where("lower(categories.title) = 'artigo' AND publish_date <= '#{Date.current}'").readonly(false)

  def self.dias_com_post
    self.order('publish_date DESC').uniq.pluck(:publish_date)
  end

  def minified_url
    if read_attribute(:minified_url).nil? || read_attribute(:minified_url) == 'error: No operations allowed after connection closed.'
      generate_minified_url
    end
    
    read_attribute(:minified_url)
  end

  def increment_visualizations
    self.visualizations = self.visualizations.to_i + 1
    self.save
  end

  private

  def generate_minified_url
      #url = Rails.application.routes.url_helpers.send("show_#{category.title.downcase}_url", URI::escape(self.slug))
      url = "http://www.buzzmonitor.pt/blog/" + URI::escape(self.slug)
      uri = URI.parse("http://zi.cx/_ws/shorten?key=zAtAmexezeqaTRaGapHEc4TaDaZESEsT&address=#{url}&tag=qualquer&type=text")

      # Shortcut
      response = Net::HTTP.get_response(uri)

      # Will print response.body
      Net::HTTP.get_print(uri)

      # Full
      http = Net::HTTP.new(uri.host, uri.port)
      response = http.request(Net::HTTP::Get.new(uri.request_uri))

      update_attribute(:minified_url, response.body.strip)
  end

end