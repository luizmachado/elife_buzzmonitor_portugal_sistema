class Ebook < ActiveRecord::Base
  attr_accessible :data, :imagem, :link, :texto, :titulo, :slug, :pdf

has_attached_file :imagem, :styles => {:home => '153x216#', :medium => '255x355#'},
    :path           => "#{ENV['s3_root']}/:attachment/:id/:style.:extension",
    :storage        => :s3,
    :bucket            => ENV['s3_bucket'],
    :s3_credentials => {
        :access_key_id     => ENV['s3_access_key_id'],
        :secret_access_key => ENV['s3_secret_access_key']
    }  
   
has_attached_file :pdf,
    :path           => "#{ENV['s3_root']}/:attachment/:id/:style.:extension",
    :storage        => :s3,
    :bucket            => ENV['s3_bucket'],
    :s3_credentials => {
        :access_key_id     => ENV['s3_access_key_id'],
        :secret_access_key => ENV['s3_secret_access_key']
    }   
    after_create :generate_minified_url

	def self.dias_com_post
    	self.order('data DESC').uniq.pluck(:data)
  	end

  	def minified_url
    if read_attribute(:minified_url).nil? || read_attribute(:minified_url) == 'error: No operations allowed after connection closed.'
      generate_minified_url
    end
    
    read_attribute(:minified_url)
  end

  private

  def generate_minified_url
      url = "http://www.buzzmonitor.pt/ebooks/e/" +  URI::escape(self.slug)
      uri = URI.parse("http://zi.cx/_ws/shorten?key=zAtAmexezeqaTRaGapHEc4TaDaZESEsT&address=#{url}&tag=qualquer&type=text")

      # Shortcut
      response = Net::HTTP.get_response(uri)

      # Will print response.body
      Net::HTTP.get_print(uri)

      # Full
      http = Net::HTTP.new(uri.host, uri.port)
      response = http.request(Net::HTTP::Get.new(uri.request_uri))

      update_attribute(:minified_url, response.body.strip)
  end


end
