class CarouselImage < ActiveRecord::Base
  attr_accessible :deleted_at, :link, :priority, :image, :usa_lightbox, :width, :height, :category_slug

  has_attached_file :image, :styles => {:home => '960x267#'},
    :path           => "#{ENV['s3_root']}/:attachment/:id/:style.:extension",
    :storage        => :s3,
    :bucket            => ENV['s3_bucket'],
    :s3_credentials => {
        :access_key_id     => ENV['s3_access_key_id'],
        :secret_access_key => ENV['s3_secret_access_key']
    }

    scope :padrao, self.where(:category_slug => nil).order('priority ASC')

    def self.including_optimus
    	self.where(:category_slug => [nil, 'optimus']).order('priority ASC')
    end
end