class Category < ActiveRecord::Base
  attr_accessible :deleted_at, :title
end
