class Ckeditor::AttachmentFile < Ckeditor::Asset
  has_attached_file :data,
                    :path           => "#{ENV['s3_root']}/:attachment/:id/:style.:extension",
				    :storage        => :s3,
				    :bucket            => ENV['s3_bucket'],
				    :s3_credentials => {
				        :access_key_id     => ENV['s3_access_key_id'],
				        :secret_access_key => ENV['s3_secret_access_key']
				    }
  
  validates_attachment_size :data, :less_than => 100.megabytes
  validates_attachment_presence :data
	
	def url_thumb
	  @url_thumb ||= Ckeditor::Utils.filethumb(filename)
	end
end
