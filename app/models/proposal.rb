# encoding: UTF-8
class Proposal < ActiveRecord::Base
  attr_accessible :company_name, :email_address, :name, :newsletter, :phone_number, :ip, :flag_captcha
  validates_confirmation_of :email_address, :message => "deve corresponder confirmação"
  validates_presence_of :email_address

  attr_accessor :captcha

  validate :captcha_validation

	def captcha_validation
		errors.add(:captcha, "inválido") if self.captcha == false
	end

	def self.verify_robot remote_ip 
  	proposals =  find(:all, :conditions => ["ip = ? AND (flag_captcha = ? OR flag_captcha = ?) ", remote_ip , true, false] , :order => "created_at DESC", :limit => 2 )
  	if !proposals.nil? && proposals.count >= 2 && (proposals.find { |proposal| proposal.flag_captcha == true }.nil?)
  		true
  	else
  		false
  	end
  end

  def first_name
    unless self.name.blank?
      name = self.name.split(" ")
      return name[0]
    else
      return ''
    end
  end

  def last_name
    unless self.name.blank?
      name = self.name.split(" ")
      if name.count > 1
        return name[1]
      else
        return ''
      end
    else
      return ''
    end
  end

end
