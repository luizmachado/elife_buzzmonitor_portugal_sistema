# encoding: UTF-8
class User < ActiveRecord::Base
  attr_accessor :captcha, :password, :password_confirmation
  attr_accessible :email, :flag_captcha, :ip, :password, :password_confirmation

  validate :captcha_validation

  validates :password, 
          # you only need presence on create
          :presence => { :on => :create },
          # allow_nil for length (presence will handle it on create)
          :length   => { :minimum => 6, :allow_nil => true },
          # and use confirmation to ensure they always match
          :confirmation => true

  def captcha_validation
    errors.add(:captcha, "inválido") if self.captcha == false
  end

  def self.verify_robot remote_ip 
    users =  find(:all, :conditions => ["ip = ? AND (flag_captcha = ? OR flag_captcha = ?) ", remote_ip , true, false] , :order => "created_at DESC", :limit => 3 )
    if !users.nil? && users.count >= 2 && (users.find { |user| user.flag_captcha == true }.nil?)
      true
    else
      false
    end
  end


end
