ActiveAdmin.register Post do
  form do |f|                         
 	f.inputs "Categoria" do       
 	  f.input :category
 	  f.input :title
 	  f.input :subtitle
 	  f.input :cover
    f.input :preview
 	  f.input :body, :as => :ckeditor, :input_html => { :width => '78%' }
 	  f.input :slug
 	  f.input :publish_date
 	end                               
 	f.buttons                         
  end

  filter :category
  filter :title
  filter :subtitle

  index do       
  	selectable_column                     
    column :category                     
    column :title        
    column :subtitle
    column :preview      
    column :publish_date           
    column :updated_at           
    column :created_at           
    default_actions                   
  end 

  show do |ad|
    attributes_table do
      row :cover do 
      	image_tag ad.cover.url, :width => '100px'
      end
      row :category
      row :title
      row :subtitle
      row :preview
      row :slug
      row :publish_date
      row :body do |post|
        raw post.body
      end
    end
  end
end
