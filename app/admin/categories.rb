ActiveAdmin.register Category do

  index do       
  	selectable_column                     
    column :id                     
    column :title        
    column :created_at           
    column :updated_at             
    default_actions                   
  end 

  filter :title

  form do |f|                         
    f.inputs "Categoria" do       
      f.input :title
    end                               
    f.buttons                         
  end 
  
end
