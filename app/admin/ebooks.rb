ActiveAdmin.register Ebook do
form do |f|   
f.inputs "Cadastro de Ebook" do                       
    f.input :titulo
    f.input :slug
    f.input :data
    f.input :imagem
    f.input :pdf
    f.input :texto, :as => :ckeditor, :input_html => { :width => '78%' }
    f.input :link
    
     end
  f.buttons                         
  end



index do       
    selectable_column 
    column :titulo
    column :texto           
    column :data
    column :imagem   
    column :pdf 
    column :link           
              
    default_actions                   
  end 
 
  show do |ad|
    attributes_table do
      row :imagem do 
        image_tag ad.imagem.url, :width => '100px'
      end
      row :titulo
    row :data
      row :link
      row :slug
      row :texto do |ebooks|
        raw ebooks.texto
      end
    end
  end
end

  
  

