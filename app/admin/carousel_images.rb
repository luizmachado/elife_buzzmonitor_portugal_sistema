# encoding: UTF-8
ActiveAdmin.register CarouselImage do

  form do |f|                         
 	f.inputs "Categoria" do       
 	  f.input :priority
 	  f.input :link
    f.input :image
    f.input :usa_lightbox
    f.input :category_slug
    f.input :width
    f.input :height
 	end                               
 	f.buttons                         
  end 

  filter :priority

  index do       
  	selectable_column                     
    column :image do |ci|
      	image_tag ci.image.url, :width => '100px'
    end
    column :priority               
    column :link
    column :usa_lightbox do |ad|
        ad.usa_lightbox ? 'Sim' : 'Não'
    end
    column :category_slug
    column :width
    column :height
    column :updated_at           
    column :created_at         
    default_actions                
  end 

  show do |ad|
    attributes_table do
      row :image do 
      	image_tag ad.image.url, :width => '100px'
      end
      row :priority
      row :link
      row :usa_lightbox do
        ad.usa_lightbox ? 'Sim' : 'Não'
      end
      row :category_slug
      row :width
      row :height
      row :updated_at
      row :created_at
    end
  end
  
end
