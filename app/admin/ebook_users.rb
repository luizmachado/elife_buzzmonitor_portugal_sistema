ActiveAdmin.register EbookUser do
form do |f|   
f.inputs "Cadastro de Usuarios Ebook" do                       
 	  f.input :name
      f.input :email
      f.input :company
 	  f.input :phone
 	  f.input :accept_newsletter
    
 	   end
 	f.buttons                         
  end



index do       
  	selectable_column 
    column :name
    column :email           
    column :company
 	column :phone   
 	column :accept_newsletter
              
    default_actions                   
  end 
 
  show do |ad|
    attributes_table do
  	  row :name
 	  row :email
      row :company
      row :phone
      row :accept_newsletter
    end
  end
end

	
  

