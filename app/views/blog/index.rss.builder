xml.instruct! :xml, version: "1.0" 
xml.rss version: "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
    xml.title "Buzzmonitor"
    xml.description "Buzzmonitor, uma ferramenta muito mais inteligente para monitorar as redes sociais."
    xml.language 'pt-BR'
    xml.link url_for :only_path => false, :controller => 'blog'

    for post in @posts
      xml.item do
        xml.title post.title
        xml.description "type" => "html" do
           xml.cdata!(post.body)
        end
        xml.pubDate post.publish_date.rfc2822
        #xml.link show_blog_url(post)
        #xml.guid show_blog_url(post)
        xml.link show_blog_path(post.slug, :only_path => false)
        xml.guid show_blog_path(post.slug, :only_path => false)
      end
    end
  end
end
