require "net/http"
require "uri"

module ApplicationHelper
	
	def parse_post_body body
		paragraphs = Nokogiri::HTML(body).xpath('/html/body/child::*').reject { |node|
				node.first_element_child.nil? && node.content.strip.size <= 4
			}

		content = paragraphs.first.content.strip.size <= 4 ? paragraphs.first.first_element_child.to_s + paragraphs[1].content : paragraphs.first

		raw truncate_html(content, :length => 400)
	end

	def compartilhamentos params
		parsed_url = params[:url].gsub( /^http*:\/\/(www\.)?/, 'http://www.')

		html = '<div class="g-plusone" data-size="medium" data-href="' +  parsed_url + '"></div>'
		
		html = html + '<a href="https://twitter.com/share" class="twitter-share-button" data-text="Buzzmonitor / ' +  params[:title] + '" data-url="' +  params[:minified_url] + '" data-counturl="' +  parsed_url + '" data-via="buzz_monitor" data-lang="pt">Tweetar</a>'

		raw( html + '<div class="fb-like" data-href="' + parsed_url + ' data-send="false" data-layout="button_count" data-width="450" data-show-faces="true"></div>' )
	end

	def compartilhamentosebooks params
		parsed_url = params[:url].gsub( /^http*:\/\/(www\.)?/, 'http://www.')

		html = '<div class="g-plusone" data-size="medium" data-href="' +  parsed_url + '"></div>'
		
		html = html + '<a href="https://twitter.com/share" class="twitter-share-button" data-text="Buzzmonitor / ' +  params[:titulo] + '" data-url="' +  params[:minified_url] + '" data-counturl="' +  parsed_url + '" data-via="buzz_monitor" data-lang="pt">Tweetar</a>'

		raw( html + '<div class="fb-like" data-href="' + parsed_url + ' data-send="false" data-layout="button_count" data-width="450" data-show-faces="true"></div>' )
	end

end