require 'test_helper'

class PropostaMailerTest < ActionMailer::TestCase
  test "nova_proposta" do
    mail = PropostaMailer.nova_proposta
    assert_equal "Nova proposta", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
