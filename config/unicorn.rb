worker_processes 3
timeout 90
preload_app true

after_fork do |server, worker|
		if defined?(ActiveRecord::Base)
			ActiveRecord::Base.establish_connection
		end
end