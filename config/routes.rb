BuzzmonitorBlogRails::Application.routes.draw do

  match "/404", :to => "pages#error_404"
  
  get "home" => 'pages#home', :as => :home

  get "home_a" => 'pages#home_a', :as => :home_a

  get "home_b" => 'pages#home_b', :as => :home_b

  get "home_c" => 'pages#home_c', :as => :home_c

  get "ebooks/:slug" => 'ebooks#detail', :as => :detail_ebook

  get "ebooks/e/:slug" => 'ebooks#detail', :as => :detail_ebook

  post "ebook_users" => 'ebooks#register', :as =>  :ebook_register

  get "download/:slug" => "ebooks#download", :as => :download_ebooks

  get "ebooks" => "ebooks#index", :as => :ebooks

  get "blog" => "blog#index", :as => :blog

  get "blog/:slug" => "blog#show", :as => :show_blog

  match "artigos" => "artigos#index", :as => :artigos
  
  match "artigos/:slug" => "artigos#show", :as => :artigo

  match "imprensa" => "imprensa#index", :as => :imprensa
  
  match "imprensa/:slug" => "imprensa#show", :as => :show_imprensa

  get "conheca" => 'pages#conheca', :as => :conheca

  get "ebooks/obrigado/:slug" => "ebooks#thankyou", :as => :thankyou_ebooks

  post "submit_proposal" => 'pages#submit_proposal', :as => :submit_proposal

  post "submit_proposal" => 'pages#submit_proposal', :as => :submit_proposal

  post "submit_newsletter" => 'pages#submit_newsletter', :as => :submit_newsletter

  post "submit_signup" => 'users#submit_signup', :as => :submit_signup

  match "proposal_tks" => "pages#proposal_tks", :as => :proposal_tks

  
  get "/" => 'pages#faq', :constraints => { :subdomain => /faq/ }
  
  ActiveAdmin.routes(self)

  devise_config = ActiveAdmin::Devise.config

  devise_config[:controllers][:passwords] = 'passwords'

  devise_for :admin_users, devise_config

  mount Ckeditor::Engine => '/ckeditor'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'pages#home'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
