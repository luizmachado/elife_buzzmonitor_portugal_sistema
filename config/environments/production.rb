require 'highrise'

BuzzmonitorBlogRails::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # Code is not reloaded between requests
  config.cache_classes = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = true
  config.static_cache_control = "public, max-age=31536000"

  # Compress JavaScripts and CSS
  config.assets.compress = true

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = false

  # Generate digests for assets URLs
  config.assets.digest = true

  # Defaults to nil and saved in location specified by config.assets.prefix
  # config.assets.manifest = YOUR_PATH

  # Specifies the header that your server uses for sending files
  # config.action_dispatch.x_sendfile_header = "X-Sendfile" # for apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for nginx

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # See everything in the log (default is :info)
  # config.log_level = :debug

  # Prepend all log lines with the following tags
  # config.log_tags = [ :subdomain, :uuid ]

  # Use a different logger for distributed setups
  # config.logger = ActiveSupport::TaggedLogging.new(SyslogLogger.new)

  # Use a different cache store in production
  # config.cache_store = :mem_cache_store

  # Enable serving of images, stylesheets, and JavaScripts from an asset server
  # config.action_controller.asset_host = "http://assets.example.com"

  # Precompile additional assets (application.js, application.css, and all non-JS/CSS are already added)
  # config.assets.precompile += %w( search.js )

  # Disable delivery errors, bad email addresses will be ignored
  # config.action_mailer.raise_delivery_errors = false

  # Enable threaded mode
  # config.threadsafe!

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation can not be found)
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners
  config.active_support.deprecation = :notify

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  # config.active_record.auto_explain_threshold_in_seconds = 0.5

  config.assets.precompile += Ckeditor.assets

  config.assets.precompile += %w[active_admin.css active_admin.js  ]


  ENV['s3_bucket'] = 'bm3blog'
  ENV['s3_root'] = 'pt'
  ENV['s3_access_key_id'] = 'AKIAIR3LGWCZ6NOF2FWQ'
  ENV['s3_secret_access_key'] = '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'
  ENV['buzzmonitor_minified_url'] = 'http://zi.cx/3hyk'

  Rails.application.routes.default_url_options[:host]= 'buzzmonitor.pt'

  ENV['analytics_base'] = 'UA-35085453-2'

  ActionMailer::Base.smtp_settings = {
      :port =>           '587',
      :address =>        'smtp.mandrillapp.com',
      :user_name =>      ENV['MANDRILL_USERNAME'],
      :password =>       ENV['MANDRILL_APIKEY'],
      :domain =>         'buzzmonitor.com.br',
      :authentication => :plain
  }
  ActionMailer::Base.delivery_method = :smtp


  ENV['mailchimp_api_key'] = "ddefa9f5217f4bf4c12c31ef43431226-us6"

  config.solicitacao_email_destino = 'ines.boulhosa@elifemonitor.com, joao.almeida@buzzmonitor.com.br, joana@elifemonitor.com, negocios@elife.com.br, negocios@elifemonitor.com, eduardo@casulloweb.com.br, andaira@casulloweb.com.br, margarida@elifemonitor.com, contacto@buzzmonitor.pt'
  config.default_send_mail = 'ines.boulhosa@elifemonitor.com, joao.almeida@buzzmonitor.com.br, joana@elifemonitor.com, negocios@elife.com.br, negocios@elifemonitor.com, eduardo@casulloweb.com.br, andaira@casulloweb.com.br, margarida@elifemonitor.com, contacto@buzzmonitor.pt'
  config.notification_error =  'eduardo@casulloweb.com.br, andaira@casulloweb.com.br'
  

  Highrise::Base.site = 'https://elifegroupeurope.highrisehq.com'
  #Highrise::Base.user = 'e3621515b9646ee3cf7debb2cdedb694'
  #Highrise::Base.user = '3e371caad9823ff5430eac15d8cf03ac'
  Highrise::Base.user = '41985100529c4f1d461dce0c593b20f6'

  config.recaptcha_public_key  = '6LflTuESAAAAAJN0l46fG8Vp2btqa0K5ZR1F-0kP'
  config.recaptcha_private_key = '6LflTuESAAAAABEp_wZoL8e9wqPtjK1OPiM5pzF-'

  ENV['app_sign_up'] = 'https://app.buzzmonitor.com.br/user/signup/external'

  
  
end
