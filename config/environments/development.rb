BuzzmonitorBlogRails::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = true

  ENV['s3_bucket'] = 'buzzmonitor'
  ENV['s3_root'] = 'pt'
  ENV['s3_access_key_id'] = 'AKIAIPVJ3OOOVLMJI5OA'
  ENV['s3_secret_access_key'] = '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'
  ENV['buzzmonitor_minified_url'] = 'http://zi.cx/3hyk'



  Rails.application.routes.default_url_options[:host]= 'localhost:3000'


  ActionMailer::Base.smtp_settings = {
    :user_name => 'casullodesign',
    :password => '!2009biz1',
    :domain => 'casulloweb.com.br',
    :address => 'smtp.sendgrid.net',
    :port => 587,
    :authentication => :plain,
    :enable_starttls_auto => true
  }
  ActionMailer::Base.delivery_method = :smtp

  ENV['mailchimp_api_key'] = '9fac762ac2b55a9b5ee9d6ad0222d8df-us4'

  Highrise::Base.site = 'https://casullo1.highrisehq.com'
  Highrise::Base.user = 'a08f86fdfa144776964867cb95d80eeb'

  config.solicitacao_email_destino = 'caiocesarca@gmail.com'
  config.default_send_mail = 'caiocesarca@gmail.com'
  config.notification_error = 'caiocesarca@gmail.com'
  
  config.recaptcha_public_key  = '6LflTuESAAAAAJN0l46fG8Vp2btqa0K5ZR1F-0kP'
  config.recaptcha_private_key = '6LflTuESAAAAABEp_wZoL8e9wqPtjK1OPiM5pzF-'

  ENV['app_sign_up'] = 'https://staging.buzzmonitor.com.br/user/signup/external'
  
end
